;;; -*- lexical-binding:t ; -*-

(use-package pyim
  :doc "A Chinese IME in Emacs"
  :homepage "https://github.com/tumashu/pyim"
  ;; :defer 2
  :hook (prog-mode . (lambda ()
                       (setq-local pyim-punctuation-dict nil)))
  :init
  ;; (liberime-start "/usr/share/rime-data" (expand-file-name "rime"
  ;;                                                          cm/cache-files-directory))
  ;; (liberime-select-schema "double_pinyin_flypy")
  (setq default-input-method "pyim"
        pyim-default-scheme 'xiaohe-shuangpin
        ;; pyim-default-scheme 'rime
        )
  (setq pyim-fuzzy-pinyin-alist nil)
  (setq pyim-page-tooltip 'posframe)
  :bind
  (:map pyim-mode-map
        ("<tab>" . pyim-page-next-page)
        ("<backtab>" . pyim-page-previous-page))
  :config
  (setq-default pyim-english-input-switch-functions '((lambda ()
                                                        (button-at (point))))))

;; (use-package insert-translated-name
;;   :ensure nil
;;   :bind
;;   (("M-SPC" . insert-translated-name)))

(provide 'init-chinese)
;;; init-chinese.el ends here
