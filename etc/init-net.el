;;; -*- lexical-binding: t; -*-

(setq telega-chat-fill-column 65)
(setq telega-emoji-use-images nil)
;; (setq telega-chat-use-markdown-version 2)

(setq telega-proxies
      '((:server "localhost" :port 10809 :enable t
         :type (:@type "proxyTypeHttp"))))

(defun walle-telega-ignore-from-uids (&rest uids)
  (lambda (msg)
    (let ((sender (telega-msg-sender msg)))
      (and (telega-user-p sender)
           (member (plist-get sender :id) uids)))))

(with-eval-after-load 'telega
  (with-eval-after-load 'company
    (add-hook 'telega-chat-mode-hook (lambda ()
                                       (make-local-variable 'company-backends)
                                       (dolist (it '(telega-company-botcmd
                                                     telega-company-emoji))
                                         (push it company-backends)))))

  (add-hook 'telega-msg-ignore-predicates
            (walle-telega-ignore-from-uids
             ;; 573046592
             ;; 179799708
             ;; 847761246                  ;窝火哥
             648005759                  ;afsdsu
             5159150107                 ;Adora_hara
             827045131                  ;OnlyNull
             432787230                  ;衰潲君
             757527463                  ;LemonMX
             1031952739
             901855030
             1266677381
             930476033))

  (telega-notifications-mode)
  (telega-appindicator-mode)
  (define-key telega-chat-mode-map [(control ?l)] #'recenter-top-bottom))

(setq telega-sticker-size '(10 . 24))

(use-package helm-telega
  :init
  (setq helm-telega-stickerset-preview-count 1)
  :after telega
  :bind
  (:map telega-chat-mode-map
        ("C-c C-s" . helm-telega-sticker-mini)
        ("C-c C-v" . helm-telega-stickerset-choose)))

(provide 'init-net)
;;; init-net.el ends here
