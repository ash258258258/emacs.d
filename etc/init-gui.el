;;; -*- lexical-binding: t; -*-

(require 'walle-ui)

;;* Set up the themes
(require 'doom-themes)
(load-theme (if (display-graphic-p)
                'doom-city-lights
              'doom-one))

(setq doom-modeline-buffer-file-name-style 'buffer-name)

(require 'doom-modeline)
(doom-modeline-mode)

(when (walle-ui-display-color-emoji?)
  (set-fontset-font t 'symbol (font-spec :family "Noto Color Emoji")
                    nil 'prepend))

(defun cm/pop-up-this-frame ()
  (x-focus-frame (selected-frame)))
