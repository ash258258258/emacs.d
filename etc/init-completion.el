;;; -*- lexical-binding:t ; -*-

;;* Company
;; (use-package company-tabnine
;;   :disabled
;;   :init
;;   (defun cm//company-tab9-executable-find ()
;;     "Like `company-tabnine--executable-path', but return nil if not found."
;;     (cl-letf (((symbol-function 'company-tabnine--error-no-binaries)
;;                 'ignore))
;;       (company-tabnine--executable-path)))

;;   (defun cm/company-tab9 (&rest args)
;;     (require 'company-tabnine)
;;     (when (cm//company-tab9-executable-find)
;;       (apply #'company-tabnine args))))

(use-package company
  :defer 2
  :init
  (setq company-tooltip-align-annotations t
        company-idle-delay 0.4
        company-echo-delay 0            ; remove annoying blinking
        company-minimum-prefix-length 2
        company-require-match nil
        company-dabbrev-ignore-case nil
        company-dabbrev-downcase nil)
  (setq company-show-numbers t)
  :bind
  (:map company-active-map
        ("TAB" . company-complete-common-or-cycle)
        ("C-t" . company-other-backend))
  :init
  ;; (setq company-backends `((company-elisp :with
  ;;                                         company-yasnippet cm/company-tab9)
  ;;                          company-bbdb
  ;;                          ,@(unless (version<= "26" emacs-version)
  ;;                              '(company-nxml company-css))
  ;;                          company-semantic
  ;;                          (company-capf :with
  ;;                                        company-yasnippet cm/company-tab9)
  ;;                          cm/company-tab9
  ;;                          company-files
  ;;                          (company-dabbrev-code
  ;;                           company-gtags company-etags company-keywords)
  ;;                          company-oddmuse company-dabbrev))
  :config
  ;; Support yas in commpany
  ;; Note: Must be the last to involve all backends
  (global-company-mode))

(walle-ui-eval-after-display-init
  (use-package company-box
    :init
    (walle-autoload-from all-the-icons
      all-the-icons-material all-the-icons-faicon)
    (autoload 'all-the-icons-material "all-the-icons")
    (autoload 'all-the-icons-faicon "all-the-icons")
    (defvar cm/company-box-icons-all-the-icons
      `((Unknown . ,(all-the-icons-material "find_in_page" :height 0.9 :v-adjust -0.15))
        (Text . ,(all-the-icons-material "text_fields" :height 0.9 :v-adjust -0.15))
        (Method . ,(all-the-icons-faicon "cube" :height 0.9 :v-adjust -0.05 :face 'all-the-icons-purple))
        (Function . ,(all-the-icons-faicon "cube" :height 0.9 :v-adjust -0.05 :face 'all-the-icons-purple))
        (Constructor . ,(all-the-icons-faicon "cube" :height 0.9 :v-adjust -0.05 :face 'all-the-icons-purple))
        (Field . ,(all-the-icons-material "straighten" :height 0.9 :v-adjust -0.15 :face 'all-the-icons-blue))
        (Variable . ,(all-the-icons-material "straighten" :height 0.9 :v-adjust -0.15 :face 'all-the-icons-blue))
        (Class . ,(all-the-icons-material "settings_input_component" :height 0.9 :v-adjust -0.15 :face 'all-the-icons-orange))
        (Interface . ,(all-the-icons-material "share" :height 0.9 :v-adjust -0.15 :face 'all-the-icons-blue))
        (Module . ,(all-the-icons-material "view_module" :height 0.9 :v-adjust -0.15 :face 'all-the-icons-blue))
        (Property . ,(all-the-icons-faicon "wrench" :height 0.9 :v-adjust -0.05))
        (Unit . ,(all-the-icons-material "settings_system_daydream" :height 0.9 :v-adjust -0.15))
        (Value . ,(all-the-icons-material "format_align_right" :height 0.9 :v-adjust -0.15 :face 'all-the-icons-blue))
        (Enum . ,(all-the-icons-material "storage" :height 0.9 :v-adjust -0.15 :face 'all-the-icons-orange))
        (Keyword . ,(all-the-icons-material "filter_center_focus" :height 0.9 :v-adjust -0.15))
        (Snippet . ,(all-the-icons-material "format_align_center" :height 0.9 :v-adjust -0.15))
        (Color . ,(all-the-icons-material "palette" :height 0.9 :v-adjust -0.15))
        (File . ,(all-the-icons-faicon "file-o" :height 0.9 :v-adjust -0.05))
        (Reference . ,(all-the-icons-material "collections_bookmark" :height 0.9 :v-adjust -0.15))
        (Folder . ,(all-the-icons-faicon "folder-open" :height 0.9 :v-adjust -0.05))
        (EnumMember . ,(all-the-icons-material "format_align_right" :height 0.9 :v-adjust -0.15 :face 'all-the-icons-blueb))
        (Constant . ,(all-the-icons-faicon "square-o" :height 0.9 :v-adjust -0.05))
        (Struct . ,(all-the-icons-material "settings_input_component" :height 0.9 :v-adjust -0.15 :face 'all-the-icons-orange))
        (Event . ,(all-the-icons-faicon "bolt" :height 0.9 :v-adjust -0.05 :face 'all-the-icons-orange))
        (Operator . ,(all-the-icons-material "control_point" :height 0.9 :v-adjust -0.15))
        (TypeParameter . ,(all-the-icons-faicon "arrows" :height 0.9 :v-adjust -0.05))
        (Template . ,(all-the-icons-material "format_align_center" :height 0.9 :v-adjust -0.15))))
    (setq company-box-icons-alist 'cm/company-box-icons-all-the-icons)
    (add-hook 'company-mode-hook #'company-box-mode)
    :config
    (setq company-box-doc-delay 2)
    (setq company-box-backends-colors nil
          company-box-max-candidates 50)

    (defun cm/company-box-icons--elisp (candidate)
      (when (derived-mode-p 'emacs-lisp-mode)
        (let ((sym (intern candidate)))
          (cond ((fboundp sym) 'Function)
                ((featurep sym) 'Module)
                ((facep sym) 'Color)
                ((boundp sym) 'Variable)
                ((symbolp sym) 'Text)
                (t . nil)))))

    (advice-add #'company-box-icons--elisp :override #'cm/company-box-icons--elisp)))


;;* YASnippet
(setq yas-snippet-dirs `(,(expand-file-name "snippets" cm/library-files-directory)))
(setq yas-wrap-around-region t)
(walle-package-add-temp-hook 'yas-minor-mode-hook
  (yas-reload-all))

(walle-add-hooks '(org-mode-hook
                   prog-mode-hook
                   markdown-mode-hook
                   gfm-mode-hook)
                 #'yas-minor-mode)

;; Hippie expand
;; (use-package hippie-expand
;;   :ensure nil
;;   :bind
;;   ("M-/" . hippie-expand)
;;   :init
;;   (setq hippie-expand-try-functions-list
;;         '(try-complete-file-name-partially
;;           try-complete-file-name
;;           try-expand-dabbrev
;;           try-expand-dabbrev-all-buffers
;;           try-expand-dabbrev-from-kill)))

(provide 'init-completion)
;;; init-completion.el ends here
