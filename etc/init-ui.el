;;; -*- lexical-binding:t ; -*-

;;* Misc
(setq-default fill-column 80)

(setq line-move-visual nil)
(setq track-eol t) ; Keep the cursor at the end of the line. Requires the `line-move-visual' is nil
(setq inhibit-compacting-font-caches t) ; Set to nil will cause performance issue under windows system

;; Truly silence startup message
(advice-add #'display-startup-echo-area-message :override #'ignore)

;; Prettify Symbol
(add-hook 'org-mode-hook #'global-prettify-symbols-mode)
(add-hook 'prog-mode-hook #'global-prettify-symbols-mode)

;; Stop the blinking cursor
(blink-cursor-mode -1)

(setq use-file-dialog nil
      use-dialog-box nil
      inhibit-startup-screen t
      inhibit-startup-echo-area-message t)

(use-package shackle
  :doc "Enforce popup rules."
  :hook (after-init . shackle-mode)
  :preface
  (defvar cm/shackle--popup-window-list nil)         ; all popup windows
  (defvar-local cm/shackle--current-popup-window nil) ; current popup window
  :config
  ;; add keyword: `autoclose'
  (put 'cm/shackle--current-popup-window 'permanent-local t)

  (defun cm/shackle-display-buffer-hack (fn buffer alist plist)
    (let ((window (funcall fn buffer alist plist)))
      (setq cm/shackle--current-popup-window window)

      (when (plist-get plist :autoclose)
        (push (cons window buffer) cm/shackle--popup-window-list))
      window))

  (defun cm/shackle-close-popup-window-hack ()
    "close current popup window via `C-g'."
    (setq cm/shackle--popup-window-list
          (cl-remove-if-not
           (lambda (x)
             (pcase-exhaustive x
               (`(,window . ,buffer)
                 (and (window-live-p window)
                      (equal (window-buffer window) buffer)))))
           cm/shackle--popup-window-list))

    ;; `C-g' can deactivate region
    (when (and (called-interactively-p 'interactive)
               (not (region-active-p)))
      (if (one-window-p)
          (let* ((window (selected-window))
                 (buffer (window-buffer window)))
            (when (equal (buffer-local-value 'cm/shackle--current-popup-window
                                             buffer)
                         window)
              (winner-undo)))
        (pcase-exhaustive (pop cm/shackle--popup-window-list)
          (`(,window . ,buffer)
            (delete-window window))))))

  (advice-add #'keyboard-quit :before #'cm/shackle-close-popup-window-hack)
  (advice-add #'shackle-display-buffer :around #'cm/shackle-display-buffer-hack)

  ;; rules
  (setq shackle-default-size 0.4
        shackle-default-alignment 'below
        shackle-default-rule nil)
  (setq shackle-rules
        '(("*help*" :select t :align 'below :autoclose t)
          ("*compilation*" :size 0.25 :align 'below :autoclose t)
          ("*Completions*" :size 0.3 :align 'below :autoclose t)
          ("*Pp eval output*" :size 0.25 :align 'below :autoclose t)
          ("*ert*" :same t)
          ("*info*" :select t :inhibit-window-quit t :same t)
          ("*Backtrace*" :select t :size 20 :align 'below)
          ("*Warnings*" :size 12 :align 'below :autoclose t)
          ("*Messages*" :size 12 :align 'below :autoclose t)
          ("*DDebug*" :size 20 :align 'below :autoclose t)
          ("^\\*.*Shell command.*\\*$" :regexp t :size 0.3 :align 'below :autoclose t)
          ("\\*[Wo]*man.*\\*" :regexp t :select t :other t :inhibit-window-quit t)
          ("*Calendar*" :select t :size 0.3 :align 'below)
          (" *undo-tree*" :select t)
          (apropos-mode :size 0.3 :align 'below :autoclose t)
          (buffer-menu-mode :size 20 :align 'below :autoclose t)
          (comint-mode :align 'below)
          (grep-mode :size 25 :align 'below :autoclose t)
          (profiler-report-mode :popup t)
          (tabulated-list-mode :align 'below)
          ("^ ?\\*" :regexp t :select t :align 'below :autoclose t))))

;; Load GUI features
(walle-ui-eval-after-display-init
  (ciremacs-require 'init-gui))

(provide 'init-ui)
;;; init-ui.el ends here
