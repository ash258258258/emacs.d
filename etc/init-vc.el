;;; -*- lexical-binding:t ; -*-

(defun cm/with-true-yes-or-no-p (orig-fn &rest args)
  (require 'advice)
  (cl-letf (((symbol-function #'yes-or-no-p)
              (ad-get-orig-definition #'yes-or-no-p)))
    (apply orig-fn args)))

(setq vc-follow-symlinks t)

(use-package magit
  :doc "Git procelain in Emacs"
  :homepage "https://github.com/magit/magit"
  :init
  (setq magit-auto-revert-mode t
        magit-slow-confirm t
        magit-clone-always-transient t)
  (setq magit-clone-default-directory (expand-file-name "~/gitrepos"))
  (setq transient-default-level 7)
  :bind
  (("M-1" . magit-status)
   ("M-2" . magit-dispatch))
  :config
  (advice-add #'magit-y-or-n-p :around #'cm/with-true-yes-or-no-p)

  (add-hook 'magit-process-find-password-functions
            'magit-process-password-auth-source))

(use-package diff-hl
  :defines (diff-hl-margin-symbols-alist desktop-minor-mode-table)
  ;; :custom-face
  ;; (diff-hl-change ((t (:inherit 'highlight))))
  ;; (diff-hl-delete ((t (:inherit 'error :inverse-video t))))
  ;; (diff-hl-insert ((t (:inherit 'success :inverse-video t))))
  :bind (:map diff-hl-command-map
              ("SPC" . diff-hl-mark-hunk))
  :hook ((after-init . global-diff-hl-mode)
         (dired-mode . diff-hl-dired-mode))
  :config
  ;; Highlight on-the-fly
  (diff-hl-flydiff-mode 1)

  ;; Set fringe style
  (setq-default fringes-outside-margins t)
  (setq diff-hl-draw-borders nil)
  ;; (if sys/mac-x-p (set-fringe-mode '(4 . 8)))

  (unless (display-graphic-p)
    (setq diff-hl-margin-symbols-alist
          '((insert . " ") (delete . " ") (change . " ")
            (unknown . " ") (ignored . " ")))
    ;; Fall back to the display margin since the fringe is unavailable in tty
    (diff-hl-margin-mode 1)
    ;; Avoid restoring `diff-hl-margin-mode'
    (with-eval-after-load 'desktop
      (add-to-list 'desktop-minor-mode-table
                   '(diff-hl-margin-mode nil))))

  ;; Integration with magit
  (with-eval-after-load 'magit
    (add-hook 'magit-post-refresh-hook #'diff-hl-magit-post-refresh)))

(use-package closql
  :config
  (when (executable-find "sqlite3")
    (require 'emacsql-sqlite3)
    (defclass closql-database (emacsql-sqlite3-connection)
      ((object-class :allocation :class)))))

(use-package diff-mode
  :bind
  (:map diff-mode-map
        ("C-0" . walle-basic-alternate-buffer)
        ("M-0" . other-window)))

(provide 'init-magit)
;;; init-magit.el ends here
