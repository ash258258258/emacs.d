;;; -*- lexical-binding: t; -*-

(setq-default tab-width 8
              indent-tabs-mode nil)

(setq paren-face-regexp (rx (any "()[]{}")))
(add-hook 'prog-mode-hook #'paren-face-mode)

(dolist (x '(prog-mode-hook conf-mode-hook))
  (add-hook x #'display-line-numbers-mode))

(global-set-key [remap comment-dwim] #'evilnc-comment-or-uncomment-lines)

(walle-add-hooks '(prog-mode-hook conf-mode-hook)
                 #'electric-indent-mode)

(add-hook 'prog-mode-hook #'flycheck-mode)

;; Default bindings use RET to do `xref-goto-xref' and TAB do
;; `xref-quit-and-goto-xref'.
(with-eval-after-load 'xref
  (define-key xref--xref-buffer-mode-map (kbd "TAB") #'xref-goto-xref)
  (define-key xref--xref-buffer-mode-map (kbd "RET") #'xref-quit-and-goto-xref))

;; CC

(with-eval-after-load 'cc-mode
  (c-toggle-electric-state -1)
  (c-toggle-auto-newline -1)
  (setq c-electric-flag nil)
  (dolist (key '("#" "{" "}" "/" "*" ";" "," ":" "(" ")" "\177"))
    (define-key c-mode-base-map key nil))
  (with-eval-after-load 'smartparens
    (sp-with-modes '(c-mode c++-mode objc-mode java-mode)
      (sp-local-pair "/*!" "*/"
                     :post-handlers '(("||\n[i]" "RET") ("[d-1]< | " "SPC"))))))

;; Rust

(walle-electric-set! 'rustic-mode
  :words '("->" "where"))

(push (cons "\\.rs\\'" 'rustic-mode) auto-mode-alist)

(setq rustic-lsp-client 'eglot)

;; JS

(setq js-indent-level 4)

(with-eval-after-load 'js
  ;; Give back control to xref
  (define-key js-mode-map [(meta ?.)] nil))

(add-to-list 'auto-mode-alist `(,(rx (? ".d") ".ts" eos) . typescript-mode))

;; Sh script

(add-to-list 'auto-mode-alist '("/PKGBUILD\\'" . sh-mode)) ;Arch Package
(add-to-list 'auto-mode-alist '("\\.service\\'" . conf-mode)) ;Systemd service
(walle-electric-set! 'sh-mode
  :words '("else" "elif" "fi" "done" "then" "do" "esac" ";;"))

;; Meson

(walle-electric-set! 'meson-mode
  :words '("endif" "endforeach" "else"))


;;* LSP

;; (setq lsp-auto-guess-root t
;;       lsp-auto-configure t
;;       lsp-diagnostic-package :flycheck)

;; ;; LSP server will send large JSON to Emacs.
;; ;; NOTE: Only take effect in 27 and above.
;; (setq read-process-output-max (* 2 1024 1024))

;; (dolist (x '(js-mode-hook typescript-mode-hook rust-mode-hook))
;;   (add-hook x #'lsp-deferred))

;; (add-hook 'java-mode-hook (lambda ()
;;                             (require 'lsp-java)
;;                             (lsp-deferred)))

;; (dolist (x '(c-mode-hook c++-mode-hook))
;;   (add-hook x (lambda ()
;;                 (require 'ccls)
;;                 (lsp-deferred))))
