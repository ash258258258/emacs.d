;;; -*- lexical-binding: t; -*-

(setq elmo-passwd-storage-type 'auth-source)

(define-mail-user-agent
    'wl-user-agent
  'wl-user-agent-compose
  'wl-draft-send
  'wl-draft-kill
  'mail-send-hook)

(setq mail-user-agent 'wl-user-agent)

(setq wl-message-ignored-field-list
      '(".")
      wl-message-visible-field-list
      '("^\\(To\\|Cc\\):"
        "^Subject:"
        "^\\(From\\|Reply-To\\):"
        "^\\(Posted\\|Date\\):"
        "^Organization:"
        "^X-\\(Face\\(-[0-9]+\\)?\\|Weather\\|Fortune\\|Now-Playing\\):"))

(setq wl-stay-folder-window t)
(setq wl-user-mail-address-list '("all_but_last@163.com"
                                  "cjpeople2013@gmail.com"))

(setq wl-template-alist
      '(("163"
         ("From" . "Zhu Zihao <all_but_last@163.com>")
         (wl-from . "Zhu Zihao <all_but_last@163.com>")
         (wl-smtp-posting-user . "all_but_last@163.com")
         (wl-smtp-posting-server . "smtp.163.com")
         (wl-smtp-authenticate-type . "plain")
         (wl-smtp-connection-type . 'ssl)
         (wl-smtp-posting-port . 465)
         (wl-local-domain . "163.com")
         (wl-message-id-domain . "smtp.163.com"))
        ("gmail"
         ("From" . "Zhu Zihao <cjpeople2013@gmail.com>")
         (wl-from . "Zhu Zihao <cjpeople2013@gmail.com>")
         (wl-smtp-posting-user . "cjpeople2013@gmail.com")
         (wl-smtp-posting-server . "smtp.gmail.com")
         (wl-smtp-authenticate-type . "plain")
         (wl-smtp-connection-type . 'starttls)
         (wl-smtp-posting-port . 587)
         (wl-local-domain . "gmail.com")
         (wl-message-id-domain . "smtp.gmail.com"))))

(with-eval-after-load 'wl
  (setq wl-message-sort-field-list
        (append wl-message-sort-field-list
                '("^Reply-To" "^Posted" "^Date" "^Organization"))))

(with-eval-after-load 'wl-folder
  (define-key wl-folder-mode-map "q" #'bury-buffer)
  (define-key wl-folder-mode-map [(control ?q)] #'wl-exit))

(setq wl-highlight-x-face-function 'x-face-decode-message-header)
