(setq smtpmail-stream-type 'ssl
      smtpmail-smtp-server "smtp.163.com"
      smtpmail-smtp-user user-mail-address)

(setq smtpmail-smtp-service 465)

;; Choose signer automatically
(setq mml-secure-openpgp-sign-with-sender t)

;; Add sign automically
(add-hook 'message-mode-hook #'mml-secure-sign-pgpmime)

(setq mail-user-agent 'mu4e-user-agent)

;; Make main buffer visible in helm/ivy
(setq mu4e-main-buffer-name "*mu4e-main*")

(setq mu4e-get-mail-command "mbsync -a")

(with-eval-after-load 'mu4e
  (define-key mu4e-main-mode-map "q" #'bury-buffer)
  (define-key mu4e-main-mode-map "Q" #'mu4e-quit))
