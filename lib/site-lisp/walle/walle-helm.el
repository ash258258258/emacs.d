;;; -*- lexical-binding: t; -*-

(require 'helm)
(require 'helm-buffers)
(require 'helm-x-files)
(require 'helm-elisp)
(require 'walle-utils)

(walle-import-from limon
  limon-all-libraries)

(defun walle-helm--enh-mini-create-source ()
  (require 'limon)
  (unless helm-source-buffers-list
    (setq helm-source-buffers-list
          (helm-make-source "Buffers" 'helm-source-buffers)))

  `(
    ;; Buffers Recentf Bookmarks
    helm-source-buffers-list
    helm-source-bookmarks
    helm-source-recentf
    ;; Elisp library
    ,(helm-build-in-buffer-source "Elisp libraries (Scan)"
       :data (lambda ()
               (setq load-path (prune-directory-list load-path))
               (limon-all-libraries))
       :fuzzy-match helm-locate-library-fuzzy-match
       :keymap helm-generic-files-map
       :search (unless helm-locate-library-fuzzy-match
                 (lambda (regexp)
                   (re-search-forward
                    (if helm-ff-transformer-show-only-basename
                        (replace-regexp-in-string
                         "\\`\\^" "" regexp)
                      regexp)
                    nil t)))
       :match-part (lambda (candidate)
                     (if helm-ff-transformer-show-only-basename
                         (helm-basename candidate) candidate))
       :filter-one-by-one (lambda (c)
                            (if helm-ff-transformer-show-only-basename
                                (cons (helm-basename c) c) c))
       :filtered-candidate-transformer helm-fuzzy-sort-fn
       :action (helm-actions-from-type-file))
    ;; Dummy sources
    helm-source-buffer-not-found
    helm-source-bookmark-set))

;;;###autoload
(defun walle-helm-enh-mini ()
  (interactive)
  (helm :sources (walle-helm--enh-mini-create-source)
        :buffer "*helm walle enh mini*"
        :ff-transformer-show-only-basename nil
        :truncate-lines helm-buffers-truncate-lines))

(provide 'walle-helm)
;;; walle-helm.el ends here
