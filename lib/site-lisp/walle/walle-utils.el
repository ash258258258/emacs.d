;;; -*- lexical-binding: t; -*-

(require 'cl-lib)
(require 'subr-x)
(require 'pcase)

(defun walle-enlist (elem)
  (if (and (listp elem)
           (not (functionp elem)))
      elem
    (list elem)))

(defmacro walle-with-gensyms (symbols &rest body)
  "Bind the SYMBOLS to fresh uninterned symbols and eval BODY."
  (declare (indent 1))
  `(let ,(mapcar (lambda (s)
                   `(,s (cl-gensym (symbol-name ',s))))
                 symbols)
     ,@body))

(defmacro walle-import-from (package &rest funcs)
  (declare (indent 1) (debug (&rest symbolp)))
  (macroexp-progn
   (mapcar (lambda (f)
             `(declare-function ,f ,(symbol-name package)))
           funcs)))

(defmacro walle-autoload-from (package &rest funcs)
  (declare (indent 1) (debug (&rest symbolp)))
  (macroexp-progn
   (mapcar (lambda (f)
             `(autoload ',f ,(symbol-name package)))
           funcs)))

(provide 'walle-utils)
;;; walle-utils.el ends here
