;;; -*- lexical-binding: t; -*-

;;;###autoload
(defun walle-generate-uuid ()
  (interactive)
  (let ((ret (sha1 (format
                    "%s%s%s%s%s%s%s%s%s"
                    (user-uid) (emacs-pid) (system-name) (user-full-name) (current-time)
                    (emacs-uptime) (buffer-string) (random) (recent-keys)))))
    (when (called-interactively-p 'any)
      (kill-new ret)
      (message "Generated UUID %s saved to kill ring." ret))
    ret))
